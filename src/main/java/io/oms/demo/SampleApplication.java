package io.oms.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.SerializationFeature;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.oms.demo.resources.OrderResource;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;

public class SampleApplication extends Application<SampleConfiguration> {

	private static final Logger LOGGER = LoggerFactory.getLogger(SampleApplication.class);

	public static void main(String[] args)
			throws Exception {
		new SampleApplication().run(args);
	}

	@Override
	public void run(SampleConfiguration configuration, Environment environment)
			throws Exception {
		environment.jersey().register(new OrderResource());

		// Swagger
		environment.jersey().register(new ApiListingResource());
		environment.jersey().register(new SwaggerSerializers());

		// XXX : uniquement disponible si l'application est exécutée sous sa forme packagée
		String version = SampleApplication.class.getPackage().getImplementationVersion();
		LOGGER.debug("Implementation Version = {}" + version);
		LOGGER.debug("Specification Version = {}" + SampleApplication.class.getPackage().getSpecificationVersion());

		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setVersion(version);
		beanConfig.setBasePath("/");
		beanConfig.setResourcePackage("io.oms.demo.resources");
		beanConfig.setTitle("Demo de Swagger");
		beanConfig.setScan(true);
	}

	@Override
	public String getName() {
		return "sample-app";
	}

	@Override
	public void initialize(Bootstrap<SampleConfiguration> bootstrap) {
		// Enable variable substitution with environment variables
		bootstrap.setConfigurationSourceProvider(
				new SubstitutingSourceProvider(
						bootstrap.getConfigurationSourceProvider(),
						new EnvironmentVariableSubstitutor(false)
					)
			);

		bootstrap.addBundle(
				new AssetsBundle("/swagger-ui-2.1.4", "/swagger", "index.html", "swagger-assets")
			);

		bootstrap.getObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
		bootstrap.getObjectMapper().setSerializationInclusion(Include.NON_EMPTY);
	}

}
