package io.oms.demo.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.oms.demo.model.Address;
import io.oms.demo.model.Order;
import io.oms.demo.model.OrderLine;
import io.oms.demo.model.SaleChannel;

/**
 * API de gestion des commandes.
 */
@Path("/orders")
@Produces(MediaType.APPLICATION_JSON)
public class OrderResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderResource.class);

	private static final List<Order> ORDERS;

	static {
		ORDERS = new ArrayList<>();
		ORDERS.add(
				Order.builder()
					.id(1L)
					.channel(SaleChannel.STORE)
					.deliveryAddress(
							Address.builder()
								.firstname("John")
								.lastname("Doe")
								.city("Paris")
								.build()
						)
					.line(
							OrderLine.builder()
								.id(1L)
								.codic("4051750")
								.quantity(1)
								.build()
						)
					.build()
			);
		ORDERS.add(
				Order.builder()
					.id(2L)
					.channel(SaleChannel.STORE)
					.deliveryAddress(
							Address.builder()
								.firstname("Steve")
								.lastname("McQueen")
								.city("Paris")
								.build()
						)
					.line(
							OrderLine.builder()
								.id(2L)
								.codic("4051750")
								.quantity(1)
								.build()
						)
					.line(
							OrderLine.builder()
								.id(3L)
								.codic("1404563")
								.quantity(2)
								.build()
						)
					.build()
			);
	}

	/**
	 * Récupération de la liste des commandes.
	 * 
	 * @return la liste des commandes
	 */
	@GET
	public List<Order> findAll() {
		return ORDERS;
	}

	/**
	 * Récupération d'une commande à partir de son identifiant.
	 * 
	 * @param id
	 *        l'identifiant de la commande recherchée.
	 * @return la commande trouvée
	 */
	@GET
	@Path("/{id}")
	public Order findById(@PathParam("id") Long id) {
		return ORDERS.stream().filter(o -> id.equals(o.getId())).findFirst().orElse(null);
	}

	/**
	 * Création d'une commande.
	 * 
	 * @param order
	 *        la commande à créer
	 */
	@POST
	public void create(Order order) {
		LOGGER.debug("Order creation in progress");
	}

}
