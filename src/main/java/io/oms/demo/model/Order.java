package io.oms.demo.model;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;

/**
 * La commande.
 */
/* === Lombok => Getters and setters, Builder ... */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Order {

	/** L'identifiant de la commande. */
	private Long id;

	/** Les lignes de la commande. */
	@NotEmpty(message = "La commande doit avoir au moins une ligne")
	@Singular
	private List<OrderLine> lines;

	/** Le canal de vente. */
	@NotNull(message = "Le canal de vente est obligatoire")
	private SaleChannel channel;

	private Address billingAddress;

	private Address deliveryAddress;

}
