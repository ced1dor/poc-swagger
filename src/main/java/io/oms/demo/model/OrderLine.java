package io.oms.demo.model;

import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Une ligne de la commande.
 */
/* === Lombok => Getters and setters, Builder ... */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderLine {

	/** L'identifiant de la ligne. */
	private Long id;

	/** Le codic. */
	@NotBlank(message = "Le codic est obligatoire")
	private String codic;

	/** La quantité. */
	private int quantity;

}
