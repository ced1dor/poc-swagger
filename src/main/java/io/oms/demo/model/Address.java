package io.oms.demo.model;

import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Une adresse.
 */
/* === Lombok => Getters and setters, Builder ... */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Address {

	/** Le prénom du contact. */
	@NotBlank(message = "Le prénom du contact est obligatoire")
	private String firstname;

	/** Le nom de famille du contact. */
	@NotBlank(message = "Le nom de famille du contact est obligatoire")
	private String lastname;

	/** La première ligne de l'adresse. */
	private String line1;

	/** La deuxième ligne de l'adresse. */
	private String line2;

	/** Le code postal. */
	@NotBlank(message = "Le code postal est obligatoire")
	private String zipcode;

	/** La commune. */
	@NotBlank(message = "La commune est obligatoire")
	private String city;

}
