package io.oms.demo.model;

public enum SaleChannel {

	DOT_COM,
	MGD,
	STORE

}