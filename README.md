# POC Swagger #

POC permettant de tester l'intégration avec Swagger depuis une base Dropwizard.

Différentes approches sont ici testées afin d'évaluer les outils existants (DRY Don't Repeat Yourself).

## Objectifs ##

* Mettre à disposition une interface permettant de décrire les APIs et de les tester facilement (Swagger UI)

* Disposer d'une intégration lightweight avec Swagger

* Documenter facilement le code sans en offusquer sa lisibilité (idéalement utilisation de la javadoc)


## Branches ##

* master : intégration de **Swagger** pour générer dynamiquement les définitions + intégration de Swagger UI pour tester les APIs

* dw-swagger : intégration de **Dropwizard Swagger** [https://github.com/federecio/dropwizard-swagger](Link URL) permettant de générer dynamiquement les définitions à partir d'annotations présentes dans le code

* swagger-doclet : intégration de **Swagger Doclet** [https://github.com/teamcarma/swagger-jaxrs-doclet](Link URL) permettant de générer les définitions lors de la construction du projet